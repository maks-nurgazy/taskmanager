package com.maksnurgazy.taskmanager.mapper;

import com.maksnurgazy.taskmanager.config.MapstructMappingConfig;
import com.maksnurgazy.taskmanager.dto.TaskDto;
import com.maksnurgazy.taskmanager.dto.TaskInputDto;
import com.maksnurgazy.taskmanager.entity.Task;
import org.mapstruct.*;

@Mapper(config = MapstructMappingConfig.class)
public interface TaskMapper {
    Task toEntity(TaskDto taskDto);

    @Mapping(target = "id", ignore = true)
    Task toEntity(TaskInputDto taskInputDto);

    TaskDto toDto(Task task);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    Task partialUpdate(TaskDto taskDto, @MappingTarget Task task);
}