package com.maksnurgazy.taskmanager.controller;

import com.maksnurgazy.taskmanager.dto.TaskDto;
import com.maksnurgazy.taskmanager.dto.TaskInputDto;
import com.maksnurgazy.taskmanager.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
@RequiredArgsConstructor
@Tag(name = "Task Management", description = "APIs for todo list")
public class TaskController {

    private final TaskService taskService;

    @GetMapping
    @Operation(summary = "Get all tasks", description = "Returns a list of all tasks.")
    @ApiResponse(
            responseCode = "200",
            description = "Successful operation",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = TaskDto.class)))
    )
    public List<TaskDto> getAllTasks() {
        return taskService.getAllTasks();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get task by ID", description = "Returns a single task by its ID.")
    @ApiResponse(
            responseCode = "200",
            description = "Successful operation",
            content = @Content(schema = @Schema(implementation = TaskDto.class))
    )
    @ApiResponse(
            responseCode = "404",
            description = "Task not found"
    )
    public ResponseEntity<TaskDto> getTaskById(
            @Parameter(description = "Task ID", required = true)
            @PathVariable Long id) {
        TaskDto task = taskService.getTaskById(id);

        return ResponseEntity.ok(task);
    }

    @PostMapping
    @Operation(summary = "Create a new task", description = "Adds a new task to the db.")
    @ApiResponse(
            responseCode = "201",
            description = "Successful operation",
            content = @Content(schema = @Schema(implementation = TaskDto.class))
    )
    public ResponseEntity<TaskDto> createTask(
            @Parameter(description = "Task details", required = true)
            @Valid @RequestBody TaskInputDto taskInputDto) {
        TaskDto createdTaskDto = taskService.addTask(taskInputDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdTaskDto);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update task by ID", description = "Updates an existing task by its ID.")
    @ApiResponse(
            responseCode = "200",
            description = "Successful operation",
            content = @Content(schema = @Schema(implementation = TaskDto.class))
    )
    @ApiResponse(
            responseCode = "404",
            description = "Task not found"
    )
    public ResponseEntity<TaskDto> updateTask(
            @Parameter(description = "Task ID", required = true)
            @PathVariable Long id,
            @Parameter(description = "Updated task details", required = true)
            @RequestBody TaskDto taskDetails) {
        TaskDto updatedTask = taskService.updateTask(id, taskDetails);
        return ResponseEntity.ok(updatedTask);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete task by ID", description = "Deletes an existing task by its ID.")
    @ApiResponse(
            responseCode = "204",
            description = "Task deleted successfully"
    )
    @ApiResponse(
            responseCode = "404",
            description = "Task not found"
    )
    public ResponseEntity<Void> deleteTask(
            @Parameter(description = "Task ID", required = true)
            @PathVariable Long id) {
        taskService.deleteTask(id);

        return ResponseEntity.noContent().build();
    }
}