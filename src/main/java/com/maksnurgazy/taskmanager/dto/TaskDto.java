package com.maksnurgazy.taskmanager.dto;

import lombok.Data;

@Data
public class TaskDto {
    private Long id;
    private String description;
    private boolean completed;
}
