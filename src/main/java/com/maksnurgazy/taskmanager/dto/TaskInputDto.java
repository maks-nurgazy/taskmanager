package com.maksnurgazy.taskmanager.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class TaskInputDto {
    @NotBlank(message = "Description cannot be blank")
    @Size(max = 255, message = "Description length must be less than or equal to 255 characters")
    private String description;

    @NotNull(message = "Completed status cannot be null")
    private boolean completed;
}
