package com.maksnurgazy.taskmanager.service.impl;

import com.maksnurgazy.taskmanager.dto.TaskDto;
import com.maksnurgazy.taskmanager.dto.TaskInputDto;
import com.maksnurgazy.taskmanager.entity.Task;
import com.maksnurgazy.taskmanager.exception.ResourceNotFoundException;
import com.maksnurgazy.taskmanager.mapper.TaskMapper;
import com.maksnurgazy.taskmanager.repository.TaskRepository;
import com.maksnurgazy.taskmanager.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;

    @Override
    public List<TaskDto> getAllTasks() {
        return taskRepository.findAll(Sort.by(Sort.Direction.ASC, "id")).stream()
                .map(taskMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public TaskDto getTaskById(Long id) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task not found"));

        return taskMapper.toDto(task);
    }

    @Override
    public TaskDto addTask(TaskInputDto taskInputDto) {
        Task task = taskMapper.toEntity(taskInputDto);
        Task savedTask = taskRepository.save(task);

        return taskMapper.toDto(savedTask);
    }

    @Override
    public TaskDto updateTask(Long id, TaskDto taskDetails) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task not found"));
        Task taskToUpdate = taskMapper.partialUpdate(taskDetails, task);
        Task updatedTask = taskRepository.save(taskToUpdate);

        return taskMapper.toDto(updatedTask);
    }

    @Override
    public void deleteTask(Long id) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Task not found"));
        taskRepository.delete(task);
    }
}
