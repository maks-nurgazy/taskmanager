package com.maksnurgazy.taskmanager.service;

import com.maksnurgazy.taskmanager.dto.TaskDto;
import com.maksnurgazy.taskmanager.dto.TaskInputDto;

import java.util.List;

public interface TaskService {
    List<TaskDto> getAllTasks();

    TaskDto getTaskById(Long id);

    TaskDto addTask(TaskInputDto taskInputDto);

    TaskDto updateTask(Long id, TaskDto taskDetails);

    void deleteTask(Long id);
}
