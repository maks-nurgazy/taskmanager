CREATE TABLE task
(
    id          BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    description VARCHAR(255),
    completed   BOOLEAN                                 NOT NULL,
    CONSTRAINT pk_task PRIMARY KEY (id)
);
