package com.maksnurgazy.taskmanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maksnurgazy.taskmanager.dto.TaskDto;
import com.maksnurgazy.taskmanager.dto.TaskInputDto;
import com.maksnurgazy.taskmanager.exception.ResourceNotFoundException;
import com.maksnurgazy.taskmanager.service.TaskService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TaskController.class)
class TaskControllerTest {
    @MockBean
    TaskService taskService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getAllTasks() throws Exception {
        TaskDto taskDto = new TaskDto();
        taskDto.setId(1L);
        taskDto.setDescription("Test Task");
        taskDto.setCompleted(false);

        when(taskService.getAllTasks())
                .thenReturn(Collections.singletonList(taskDto));

        mockMvc.perform(get("/tasks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].description").value("Test Task"))
                .andExpect(jsonPath("$[0].completed").value(false));
    }

    @Test
    void getAllTasks_emptyList() throws Exception {
        when(taskService.getAllTasks())
                .thenReturn(Collections.emptyList());

        mockMvc.perform(get("/tasks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", Matchers.hasSize(0)));
    }

    @Test
    void getTaskById() throws Exception {
        TaskDto mockTask = new TaskDto();
        mockTask.setId(1L);
        mockTask.setDescription("Sample Task");
        mockTask.setCompleted(false);

        when(taskService.getTaskById(1L)).thenReturn(mockTask);

        mockMvc.perform(get("/tasks/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.description").value("Sample Task"))
                .andExpect(jsonPath("$.completed").value(false));
    }

    @Test
    void getTaskById_taskNotFound() throws Exception {
        when(taskService.getTaskById(1L)).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(get("/tasks/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void createTask() throws Exception {
        TaskInputDto taskInputDto = new TaskInputDto();
        taskInputDto.setDescription("New Task");
        taskInputDto.setCompleted(false);

        TaskDto createdTaskDto = new TaskDto();
        createdTaskDto.setId(1L);
        createdTaskDto.setDescription("New Task");
        createdTaskDto.setCompleted(false);

        when(taskService.addTask(any(TaskInputDto.class))).thenReturn(createdTaskDto);

        mockMvc.perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{ \"description\": \"New Task\", \"completed\": false }"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.description").value("New Task"))
                .andExpect(jsonPath("$.completed").value(false));
    }

    @Test
    public void createTask_validationCheck() throws Exception {
        TaskInputDto invalidTaskInputDto = new TaskInputDto();
        invalidTaskInputDto.setDescription("");
        invalidTaskInputDto.setCompleted(false);

        mockMvc.perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidTaskInputDto)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].field").exists())
                .andExpect(jsonPath("$[0].message").exists())
                .andExpect(jsonPath("$[0].rejectedValue").exists())
                .andExpect(jsonPath("$[0].field").value("description"))
                .andExpect(jsonPath("$[0].message").value("Description cannot be blank"))
                .andExpect(jsonPath("$[0].rejectedValue").value(""));

        verify(taskService, never()).addTask(any(TaskInputDto.class)); // check addTask is not called
    }

    @Test
    void updateTask() throws Exception {
        TaskDto updatedTaskDto = new TaskDto();
        updatedTaskDto.setId(1L);
        updatedTaskDto.setDescription("Updated Task Description");
        updatedTaskDto.setCompleted(true);

        when(taskService.updateTask(eq(1L), any(TaskDto.class))).thenReturn(updatedTaskDto);

        mockMvc.perform(put("/tasks/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"description\": \"Updated Task Description\", \"completed\": true}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.description").value("Updated Task Description"))
                .andExpect(jsonPath("$.completed").value(true));
    }

    @Test
    void updateTask_taskNotFound() throws Exception {
        when(taskService.updateTask(eq(1L), any(TaskDto.class))).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(put("/tasks/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"description\": \"Updated Task Description\", \"completed\": true}"))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteTask() throws Exception {
        doNothing().when(taskService).deleteTask(1L);

        mockMvc.perform(delete("/tasks/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteTask_taskNotFound() throws Exception {
        doThrow(ResourceNotFoundException.class).when(taskService).deleteTask(1L);

        mockMvc.perform(delete("/tasks/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}