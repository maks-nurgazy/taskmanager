# Task Manager Application

This project is a simple RESTful API for managing tasks using Java and Spring Boot.

## Technologies Used

- Java 17
- Spring Boot
- Maven (for dependency management)
- PostgreSQL (as the database)

## Table of Contents

1. [Running the Application](#running-the-application)
    - [With Docker Compose](#with-docker-compose)
    - [Without Docker](#without-docker)
2. [Running Tests](#running-tests)
3. [API Documentation](#api-documentation)
4. [Docker Cleanup](#docker-cleanup)
5. [Notes](#notes)

## Running the Application

### With Docker Compose

1. Make sure you have Docker and Docker Compose installed on your machine.
2. Clone this repository.
3. Navigate to the project directory.

    ```bash
    cd TaskManager
    ```

4. Build and run the application using Docker Compose:

    ```bash
    docker-compose up --build
    ```

5. The application will start with the 'dev' profile active, and you can access it at `http://localhost:8080/taskmanagement/api/v1/swagger-ui/index.html#/`.

### Without Docker

1. Make sure you have Java 17 and Maven installed on your machine.
2. Clone this repository.
3. Navigate to the project directory.

    ```bash
    cd TaskManager
    ```

4. Build the application using Maven:

    ```bash
    mvn clean package
    ```

5. Run the application with the 'dev' profile active:

    ```bash
    java -jar -Dspring.profiles.active=dev target/taskmanagement.jar
    ```

6. The application will start with the 'dev' profile active, and you can access it at `http://localhost:8080/taskmanagement/api/v1/swagger-ui/index.html#/`.

## Running Tests

To run tests, use Maven:

```bash
mvn test
```

Below the result of the running tests
![img.png](img.png)